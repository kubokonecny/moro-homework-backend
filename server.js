var express = require('express'),
    app = express(),
    port = 8080,
    bodyParser = require('body-parser');
    cors = require('cors');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

var routes = require('./api/routes');
routes(app);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);